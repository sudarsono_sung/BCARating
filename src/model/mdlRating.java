package model;

public class mdlRating {
	public String SequenceID; //varchar2(21) - Primary Key - auto increment ex : TRA-20180508-00000001
	public String WSID; //varchar2(50) - can not be null
	public String SerialNumber; //varchar2(50) can not be null
	public String StartTime; //timestamp
	public String EndTime; //timestamp
	public Integer Rating; //Integer
	public String Comment1; //varchar2(200)
	public String Comment2; //varchar2(200)
	public String Status;
	public String RecommendationProductCode;
	public String RecommendationProductName;
	public String RecommendationProductResponse;
	public String ReferenceNo;
	public String RecommendationCampaignCode;
	public String RecommendationCampaignName;
}
