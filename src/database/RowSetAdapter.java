package database;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import adapter.LogAdapter;
import oracle.jdbc.OracleConnection;

public class RowSetAdapter {
    final static Logger logger = LogManager.getLogger(RowSetAdapter.class);

    public static OracleConnection getConnectionWL() {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	OracleConnection conn = null;
	DataSource ds = null;
	try {
	    // Get the base naming context from web.xml
	    Context weblogicparam = (Context) new InitialContext().lookup("java:comp/env");
	    // Get a single value from web.xml
	    String datasourceparam = (String) weblogicparam.lookup("param_datasource");

	    Context context = new InitialContext();
	    ds = (DataSource) context.lookup(datasourceparam);
	    conn = (OracleConnection) ds.getConnection();
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "/selvi-rating", "POST", "function: "
		    + functionName, "", "", ex.toString()), ex);
	}
	return conn;
    }

}
